class ApplicationMailer < ActionMailer::Base
  # default from: 'from@example.com'
  default from: "Sample App <noreply@example.com>"
  layout 'mailer'
end